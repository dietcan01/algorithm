<?php
$menu = [
    ["id"=>1, "title" => "Thể thao", "parent_id"=>0],
    ["id"=>2, "title" => "Xã hội", "parent_id"=>0],
    ["id"=>3, "title" => "Thể thao trong nước", "parent_id"=>1],
    ["id"=>4, "title" => "Giao thông", "parent_id"=>2],
    ["id"=>5, "title" => "Môi trường", "parent_id"=>2],
    ["id"=>6, "title" => "Thể thao quốc tế", "parent_id"=>1],
    ["id"=>7, "title" => "Môi trường đô thị", "parent_id"=>5],
    ["id"=>8, "title" => "Giao thông ùn tắc", "parent_id"=>4]
];

function showCategories($menu, $parent_id = 0, $hierarchical = '')
{
    foreach ($menu as  $listMenu)
    {
        if ($listMenu['parent_id'] == $parent_id)
        {
            echo '<li style="margin:10px;
            font-size: 20px; color: blue;
            font-weight: 700;">';
                echo $hierarchical . $listMenu['title'];
            echo '</li>';
            showCategories($menu, $listMenu['id'], $hierarchical.'--');
        }
    }
}
?>
<ul style="list-style:none;
    border:1px solid blue;
    width: 250px; margin:auto;">
  <?php showCategories($menu); ?>  
</ul>